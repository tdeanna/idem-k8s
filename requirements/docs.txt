sphinx>=5.1.1
sphinx-design
furo>=2022.6.21
sphinx-copybutton>=0.5.0
Sphinx-Substitution-Extensions>=2022.2.16
sphinx-notfound-page>=0.8.3
kubernetes==23.3.0
deepdiff==5.8.0
jmespath==1.0.0

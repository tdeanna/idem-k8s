import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_deployment(hub, ctx):
    k8s_deployment_temp_name = "idem-test-k8s-deployment-" + str(uuid.uuid4())
    metadata_name = k8s_deployment_temp_name
    metadata = {"name": metadata_name, "namespace": "default"}
    spec = {
        "replicas": 3,
        "selector": {"match_labels": {"app": "nginx"}},
        "strategy": {
            "rolling_update": {"max_surge": "25%", "max_unavailable": "25%"},
            "type": "RollingUpdate",
        },
        "template": {
            "metadata": {"labels": {"app": "nginx"}},
            "spec": {
                "containers": [
                    {
                        "image": "nginx:1.14.2",
                        "image_pull_policy": "IfNotPresent",
                        "name": "nginx",
                        "ports": [{"container_port": 80, "protocol": "TCP"}],
                        "termination_message_path": "/dev/termination-log",
                        "termination_message_policy": "File",
                    }
                ],
                "restart_policy": "Always",
            },
        },
    }

    # create k8s_deployment with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.k8s.apps.v1.deployment.present(
        test_ctx, name=k8s_deployment_temp_name, metadata=metadata, spec=spec
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create k8s.apps.v1.deployment '{k8s_deployment_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_deployment(
        metadata,
        metadata_name,
        ret.get("new_state"),
        spec,
        "resource_id_known_after_present",
    )

    # create real k8s_deployment
    ret = await hub.states.k8s.apps.v1.deployment.present(
        ctx, name=k8s_deployment_temp_name, metadata=metadata, spec=spec
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created k8s.apps.v1.deployment '{k8s_deployment_temp_name}'" in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_deployment(metadata, metadata_name, ret.get("new_state"), spec)

    resource_id = ret.get("new_state").get("resource_id")

    # Describe k8s_deployment
    describe_ret = await hub.states.k8s.apps.v1.deployment.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "k8s.apps.v1.deployment.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "k8s.apps.v1.deployment.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_deployment(metadata, metadata_name, described_resource_map, spec)

    # update k8s_deployment with test flag
    updated_spec = copy.deepcopy(spec)
    updated_spec["replicas"] = 2
    ret = await hub.states.k8s.apps.v1.deployment.present(
        test_ctx,
        name=k8s_deployment_temp_name,
        metadata=metadata,
        spec=updated_spec,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update k8s.apps.v1.deployment '{k8s_deployment_temp_name}'"
        in ret["comment"]
    )

    assert_deployment(metadata, metadata_name, ret.get("old_state"), spec)
    assert_deployment(metadata, metadata_name, ret.get("new_state"), updated_spec)

    # real update k8s_deployment
    updated_spec = copy.deepcopy(spec)
    updated_spec["replicas"] = 2
    ret = await hub.states.k8s.apps.v1.deployment.present(
        ctx,
        name=k8s_deployment_temp_name,
        metadata=metadata,
        spec=updated_spec,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated k8s.apps.v1.deployment '{k8s_deployment_temp_name}'" in ret["comment"]
    )
    assert_deployment(metadata, metadata_name, ret.get("old_state"), spec)
    assert_deployment(metadata, metadata_name, ret.get("new_state"), updated_spec)

    # Delete k8s_deployment with test flag
    ret = await hub.states.k8s.apps.v1.deployment.absent(
        test_ctx,
        name=k8s_deployment_temp_name,
        resource_id=resource_id,
        metadata=metadata,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete k8s.apps.v1.deployment '{k8s_deployment_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert resource_id == resource.get("resource_id")
    assert_deployment(metadata, metadata_name, resource, updated_spec)

    # Delete k8s_deployment with real account
    ret = await hub.states.k8s.apps.v1.deployment.absent(
        ctx, name=k8s_deployment_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted k8s.apps.v1.deployment '{k8s_deployment_temp_name}'" in ret["comment"]
    )
    assert_deployment(metadata, metadata_name, ret.get("old_state"), updated_spec)

    # Deleting k8s_deployment again should be an no-op
    ret = await hub.states.k8s.apps.v1.deployment.absent(
        ctx, name=k8s_deployment_temp_name, resource_id=resource_id, metadata=metadata
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"k8s.apps.v1.deployment '{k8s_deployment_temp_name}' already absent"
        in ret["comment"]
    )


def assert_deployment(metadata, metadata_name, resource, spec, resource_id=None):
    assert metadata.get("namespace") == resource.get("metadata").get("namespace")
    assert metadata.get("name") == resource.get("metadata").get("name")
    assert spec == resource.get("spec")
    if resource_id:
        assert resource_id == resource.get("resource_id")
    else:
        assert metadata_name == resource.get("resource_id")

nginx-ds:
  k8s.apps.v1.daemon_set.present:
    - metadata:
        name: nginx-ds
        namespace: default
    - spec:
        selector:
          match_labels:
            name: nginx
        update_strategy:
          rolling_update:
            max_unavailable: 1
          type: RollingUpdate
        template:
          metadata:
            labels:
              name: nginx
          spec:
            containers:
            - image: nginx:1.14.2
              image_pull_policy: IfNotPresent
              name: nginx
              resources:
                limits:
                  memory: 200Mi
                requests:
                  cpu: 100m
                  memory: 200Mi
              termination_message_path: /dev/termination-log
              termination_message_policy: File
            restart_policy: Always
